""" Створіть змінну name, age, в якій зберігатиметься ім'я
та вік користувача, введене з клавіатури. Виведіть у консоль
“My name is and I am ”, але зробіть рішення таким,
щоб використовувалася конкатенація рядків (через +) та приведення типу
числа до рядка (str(...)). Тобто, щоб якщо замінити змінні name і age,
то в методі print() нічого не потрібно було би змінювати.
Наприклад, a = 15, print(“Value =” + str(a)) і буде “Value = 15”.
"""

name = input("Input your name:")
age = int(input("Input your age:"))
print('My name is', name, 'and I am', str(age))  # Варіант 1

Value1 = 'My name is '
Value2 = ' and I am '
Value = Value1 + name + Value2 + str(age)
print(Value)  # Варіант 2

# name = input("Input your name:")  # Працює аналогічно
# age = input("Input your age:")
# print('My name is', name, 'and I am', age)
